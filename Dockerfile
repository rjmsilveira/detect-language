FROM openjdk:8-jdk-slim 

LABEL maintainer=rjmsilveira@gmail.com
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name="rjmsilveira/language-detect"
LABEL org.label-schema.description="Language detection using nlp"
LABEL org.label-schema.url="https://www.ricardosilveira.pt"
LABEL org.label-schema.vcs-url="https://www.ricardosilveira.pt/container-support"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vendor="WSO2"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.docker.cmd="java -jar /webapp-runner/main/target/webapp-runner-main.jar nlp_updated.war  --port 8980"

RUN apt update && \
    apt install -y git wget && \
    rm -rf /var/lib/apt/lists/*

RUN wget https://mirrors.up.pt/pub/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz -O /apache-maven-3.6.3.tar.gz && \
    tar -xvf /apache-maven-3.6.3.tar.gz && \
    cd /apache-maven-3.6.3

ENV PATH=$PATH:/apache-maven-3.6.3/bin

RUN git clone https://github.com/jsimone/webapp-runner.git /webapp-runner && \
    cd /webapp-runner && \
    mvn package

EXPOSE 8980

COPY nlp_updated.war /nlp_updated.war

ENTRYPOINT [ "java", "-jar", "/webapp-runner/main/target/webapp-runner-main.jar", "/nlp_updated.war", "--port", "8980" ]
