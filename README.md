# Detect Language web service

## What does it do?
It's a web service that receives text as input and returns the probability of the languages it contains

It runs fully locally as it uses a set of files to determin the probability of each language

The main process exists inside the `war` file and its called `DetectLanguage.py` and is supported by the language files inside `WEB-INF/profiles` folder

## Source
The source of the file is more or less unknown as I found it years ago but don't really know the place where I got it

## Language codes supported
af, ar, bg, bn, cs, da, de, el, en, es, et, fa, fi, fr, gu, he, hi, hr, hu, id, it, ja, kn, ko, lt, lv, mk, ml, mr, ne, nl, no, pa, pl, pt, ro, ru, sk, sl, so, sq, sv, sw, ta, te, th, tl, tr, uk, ur, vi, zh-cn, zh-tw

## How to test locally?
First build the image:

`docker build . -t language-detect`

In one terminal, start the docker:

`docker run --rm --name detect-language -p 8980:8980 language-detect`

In another terminal execute the following:

`curl -X POST -H "application/x-www-form-urlencoded" localhost:8980/DetectLanguage.py -d 'text="o gato roeu a rolha do rei da russia"'`

And it will return the probability as a response:

`{"pt":0.999995954829807}` where it is 99% to be Portuguese, or it can return multiple languages like `{"it":0.4285729954330938,"es":0.5714266592139121}` where it mentions that its more probable to be Spanish than Italian


## How to unpack/pack the war file used?
Unzip it like so: `unzip nlp_updated.war`

Zip it again after you made the changes: `zip -r nlp_updated.war META-INF build WEB-INF DetectLanguage.py`
